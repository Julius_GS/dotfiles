from openrgb import OpenRGBClient
from openrgb.utils import RGBColor, DeviceType

client = OpenRGBClient()

client.clear()

for i in range(5,8):
    for device in client.get_devices_by_type(i):
        device.set_color(RGBColor(85, 85, 255))

