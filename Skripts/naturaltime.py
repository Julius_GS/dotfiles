#!/usr/bin/env python
# encoding=utf8
import os
import math
def naturalhour(h):
    if h == 0:
        return "zwölf"
    if h == 1:
        return "eins"
    if h == 2:
        return "zwei"
    if h == 3:
        return "drei"
    if h == 4:
        return "vier"
    if h == 5:
        return "fünf"
    if h == 6:
        return "sechs"
    if h == 7:
        return "sieben"
    if h == 8:
        return "acht"
    if h == 9:
        return "neun"
    if h == 10:
        return "zehn"
    if h == 11:
        return "elf"
    if h == 12:
        return "zwölf"
    if h == 13:
        return "eins"
def getnaturaltime(h,m):
    m = 5*round(m/5)
    appendage = ""
    if h >= 12:
        appendage += "+"
    else:
        appendage += ""
    h = h%12
    nath = str(naturalhour(h))
    nnath = str(naturalhour(h+1))
    # print("nnath",nnath)
    # print("m:",m)
    if (h == 1 or h == 13) and m == 0:
        return "ein Uhr"
    else:
        if m == 0:
            return nath+ " Uhr"+appendage
        elif m == 5:
            return "fünf nach "+nath+appendage
        elif m == 10:
            return "zehn nach "+nath+appendage
        elif m == 15:
            return "viertel nach "+nath+appendage
        elif m == 20:
            return "zwanzig nach "+nath+appendage
        elif m == 25:
            return "fünf vor halb "+nnath+appendage
        elif m == 30:
            return "halb "+nnath+appendage
        elif m == 35:
            return "fünf nach halb "+nnath+appendage
        elif m == 40:
            return "zwanzig vor "+nnath+appendage
        elif m == 45:
            return "viertel vor "+nnath+appendage
        elif m == 50:
            return "zehn vor "+nnath+appendage
        elif m == 55:
            return "fünf vor "+nnath+appendage
        elif m == 60:
            return str(nnath)+" Uhr"+appendage

def getminandh():
    curmin = int(os.popen("date +%M").read())
    curh = int(os.popen("date +%H").read())
    return curh, curmin
def main():
    h,m = getminandh()
    # print(getnaturaltime(curh,curmin))
    # h = 18
    # m = 2
    # print(f"Time is {h}:{m} ")
    return getnaturaltime(h,m)
    print(getnaturaltime(h,m))
if __name__ == "__main__":
    main()
