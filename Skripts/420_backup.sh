#! /bin/zsh
str1="04:20"
str2="04:19"
a=$(date +%I:%M)
curh=$(date +%I)
curmin=$(date +%M)
if [ "$a" = "$str1" ]; then
    echo "🌿🌿🌿 -- 420 blaze it -- 🌿🌿🌿"
elif [ "$a" = "$str2" ]; then
    echo "Give me a fucking break, one minute left. One minute fucking left. You’re gonna give me fucking one minute?"
else
    if (($curh <= 3)); then
        if (($curmin <= 20)); then
            remh=$((4-curh))
            remmin=$((20-curmin))
        else
            remh=$((3-curh))
            remmin=$((20+60-curmin))
        fi
    elif (($curh == 4)); then
        if (($curmin <= 20)); then
            remh=$((0))
            remmin=$((20-curmin))
        else
            remh=$((11))
            remmin=$((60+20-curmin))
        fi
    else
        if (($curmin <= 20)); then
            remh=$((12+4-curh))
            remmin=$((20-curmin))
        else
            remh=$((12+3-curh))
            remmin=$((20+60-curmin))
        fi
    fi
    printf "%02d" $remh
    printf " h "
    printf "%02d" $remmin
    printf " min verbleiben"
    # "$remh h $remmin min verbleiben\n"
fi
