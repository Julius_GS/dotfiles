#! /bin/bash
id=$(xdotool search --class scratchterm | sort | head -n 1)

if [ "$id" != "" ]
    then
    bspc node "$id" --flag hidden -f
fi

