#! /bin/bash
id=$(xdotool search --class mathterm | sort | head -n 1)

if [ "$id" != "" ]
    then
    bspc node "$id" --flag hidden -f
fi
