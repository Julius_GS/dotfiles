#!/bin/zsh
a=$(bspc query -D -d '.active' --names | sort -V)
printf "alle ws:\n $a\n"
focusedws=$(bspc query -D -d '.focused' --names)
printf "focused ws:\n$focusedws\n"
numws=$(echo $a | wc -l)
for i in {1..$(($numws-1))};
    do printf "i ist $i\n";
    string1=$(echo $a | sed "${i}q;d")
    printf "string1: $string1\n"
    string2=$(echo $focusedws)
    printf "string2: $string2\n"
    if [ $string1 = $string2 ]; then
        targetws=$(echo $a | sed "$((${i}+1))q;d")
        printf "hier if\n"
        break
    fi
    printf "hier else\n"
    targetws=$(echo $a | sed "1q;d")
done;
bspc node -d $targetws
