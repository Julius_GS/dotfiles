#!/bin/bash

options=""

for i in {5..100..5}
do
    if (($i == 5)); then
        options="$i%"
    else
        options="$options\n$i%"
    fi
done

wofi_command="wofi -p Select -d --width 1920 --height 1080 --x 0 --y 0"

chosen="$(echo -e "$options" | $wofi_command)"

if [ -n "$chosen" ]; then
    exec brightnessctl set $chosen
fi
