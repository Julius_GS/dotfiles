# !/bin/bash

folderPath="$HOME/Pictures/backgrounds"

while true; do
    path="$folderPath/$(ls $folderPath | shuf -n 1)"

    swaybg -i $path -m stretch -o '*' &

    pid=$!

    sleep 60

    kill $pid
done
