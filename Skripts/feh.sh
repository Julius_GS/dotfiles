# !/bin/bash

folderPath="$HOME/Pictures/backgrounds";

while true; do
    path="$folderPath/$(ls $folderPath | shuf -n 1)"

    feh --bg-scale $path

    sleep 60
done
