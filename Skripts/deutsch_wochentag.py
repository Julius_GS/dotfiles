#!/usr/bin/env python
# encoding=utf8
import os
def main():
    wday = int(os.popen("date +%u").read())
    date = os.popen("date +%d.%m.%Y").read()
    if wday == 1:
        daystr="Montag"
    elif wday == 2:
        daystr = "Dienstag"
    elif wday == 3:
        daystr = "Mittwoch"
    elif wday == 4:
        daystr = "Donnerstag"
    elif wday == 5:
        daystr = "Freitag"
    elif wday == 6:
        daystr = "Samstag"
    elif wday == 7:
        daystr = "Sonntag"
    outstring = daystr + ", der " + date[0:len(date)-1]
    return outstring
# if __name__ == "__main__":
#     main()
