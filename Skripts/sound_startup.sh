# !/bin/bash

exec -a alsa-in alsa_in -j micro -d hw:PCH,0 > /dev/null 2>&1 &
pid1=$(ps -e | grep alsa_in | $HOME/Projects/nth/target/debug/nth 1)
echo "Started mic successfully with $pid1"

exec -a alsa-out alsa_out -j speaker -d hw:USB,0 > /dev/null 2>&1 &
pid2=$(ps -e | grep alsa_out | $HOME/Projects/nth/target/debug/nth 1)
echo "Started speaker successfully with $pid2"

read -n 1 -s

kill $pid1
kill $pid2
