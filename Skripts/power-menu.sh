#!/bin/bash

shutdown="shutdown"
reboot="reboot"
lock="lock"
suspend="suspend"
hibernate="hibernate";
logout="logout"

wofi_command="wofi -i -p Select -d --width 1920 --height 1080 --x 0 --y 0"

options="$shutdown\n$reboot\n$lock\n$suspend\n$hibernate\n$logout"

chosen="$(echo -e "$options" | $wofi_command)"

case $chosen in
    $shutdown)
        exec loginctl poweroff
        ;;
    $reboot)
        exec loginctl reboot
        ;;
    $lock)
        exec strat arch swaylock
        ;;
    $suspend)
        exec loginctl suspend
        ;;
    $hibernate)
        exec loginctl hibernate
        ;;
    $logout)
        exec swaymsg exit
        ;;
    *)
esac

