# !/bin/bash


background='#282a36'    #   '40 42 54'
current-line='#44475a'  #   '68 71 90'
selection='#44475a'     #   '68 71 90'
foreground='#f8f8f2'    #   '248 248 242'
comment='#6272a4'       #   '98 114 164'
cyan='#8be9fd'          #   '139 233 253'
green='#50fa7b'         #   '80 250 123'
orange='#ffb86c'        #   '255 184 108'
pink='#ff79c6'          #   '255 121 198'
purple='#bd93f9'        #   '189 147 249'
red='#ff5555'           #   '255 85 85'
yellow='#f1fa8c'        #   '241 250 140'
