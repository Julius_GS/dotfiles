#!/bin/bash

yes="yes"
no="no"

wofi_command="wofi -p Default? -d --width 1920 --height 1080 --x 0 --y 0"

options="$yes\n$no"

chosen="$(echo -e "$options" | $wofi_command)"

if [ $chosen = $yes ]; then
    swaybg -i $HOME/Pictures/background.png -m stretch -o '*' &
else
    exec $HOME/Skripts/refreshWallpaper.sh
fi
