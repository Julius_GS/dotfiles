#!/usr/bin/env bash

export PYTHONIOENCODING=utf8

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch bar1 and bar2
echo "---" | tee -a /tmp/polybar1.log #/tmp/polybar2.log
#polybar example >>/tmp/polybar1.log 2>&1 &
# polybar bar2 >>/tmp/polybar2.log 2>&1 &


# if type "xrandr"; then
#   for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
hostname=$(cat /etc/hostname)
# MONITOR=HDMI-0 polybar --reload haupt&
if xrandr | rg "DP-3-9 connected"; then
    MONITOR=DP-3-9 polybar --reload haupt&
    MONITOR=DP-3-8 polybar --reload haupt&
else
    MONITOR=HDMI-1 polybar --reload haupt&
    MONITOR=eDP-1 polybar --reload haupt&
fi
echo "Bars launched..."
