#!/bin/sh

grep -v "#" $HOME/.config/dmenu/emoji_list | dmenu-wl -i | awk '{print $1}' | tr -d '\n' | wl-copy # -selection clipboard

pgrep -x dunst >/dev/null && notify-send "$(xclip -o -selection clipboard) Copied!"
