    -- Basic settings
vim.o.number = true -- Enable line numbers
vim.o.tabstop = 4 -- Number of spaces a tab represents
vim.o.shiftwidth = 4 -- Number of spaces for each indentation
vim.api.nvim_create_autocmd("FileType", {
    pattern = { "r", "rmd" },
    callback = function()
        vim.bo.tabstop = 2      -- Number of spaces tabs count for
        vim.bo.shiftwidth = 2   -- Number of spaces for each indentation level
        vim.bo.expandtab = true -- Use spaces instead of tabs
    end,
})
vim.o.expandtab = true -- Convert tabs to spaces
vim.o.tabstop = 4
vim.o.shiftwidth=4
vim.o.smartindent = true -- Automatically indent new lines
vim.o.wrap = true -- Disable line wrapping
vim.o.tw = 85
vim.o.cursorline = false -- Highlight the current line
vim.o.termguicolors = true -- Enable 24-bit RGB colors

-- Syntax highlighting and filetype plugins
vim.cmd('syntax enable')
vim.cmd('filetype plugin indent on')
vim.g.python3_host_prog = '~/.venvs/nvim/bin/python'

require("config.lazy")
require("config.keybinds")
vim.cmd[[colorscheme dracula]]
vim.opt.signcolumn = "yes"
