call plug#begin('~/.local/share/plugged')
" ------ Syntax highlighting -----

Plug 'cespare/vim-toml'
Plug 'SirVer/ultisnips'
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsEditSplit="vertical"
Plug 'ervandew/supertab'
Plug 'ycm-core/YouCompleteMe'
" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'
" autocmd FileType tex 

let g:ycm_filetype_blacklist = {
    \ 'tagbar': 1,
    \ 'notes': 1,
    \ 'markdown': 1,
    \ 'netrw': 1,
    \ 'unite': 1,
    \ 'text': 1,
    \ 'vimwiki': 1,
    \ 'pandoc': 1,
    \ 'infolog': 1,
    \ 'leaderf': 1,
    \ 'mail': 1,
    \ 'tex': 1,
    \ 'latex': 1,
    \ 'cpp': 1,
    \}
let g:ycm_filetype_specific_completion_to_disable = {
      \ 'gitcommit': 1,
      \ 'tex': 1,
      \ 'latex': 1
      \}
Plug 'honza/vim-snippets'
Plug 'JuliaEditorSupport/julia-vim'

" ----- Navigation ------
Plug 'scrooloose/nerdtree' |
    \ Plug 'xuyuanp/nerdtree-git-plugin' |
    \ Plug 'ryanoasis/vim-devicons' |

" ----- Other -----
Plug 'vim-airline/vim-airline'
Plug 'liuchengxu/vim-which-key', { 'on': ['WhichKey', 'WhichKey!'] }
Plug 'scrooloose/nerdcommenter'
Plug 'prettier/vim-prettier', { 'do': 'yarn install' }

" LaTeX-Support
Plug 'lervag/vimtex'
let g:vimtex_view_general_viewer = 'zathura'
let g:vimtex_compiler_latexmk = {
    \ 'options' : [
    \   '-pdf',
    \   '-shell-escape',
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \ ],
    \}
set conceallevel=1
let g:tex_conceal='abdmg'
let g:tex_flavor = 'xelatex'

" Theme
Plug 'dracula/vim',{ 'as': 'dracula' }
" Initialize plugin system
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
call plug#end()

source $HOME/.config/nvim/init.vim.local


" ----- Colorscheme -----
colorscheme dracula
let g:airline_theme='dracula'
let g:dracula_colorterm = 0


" ----- General settings -----
syntax enable

set termguicolors
set guicursor=
set tabstop=4       " The width of a TAB is set to 4. Still it is a \t. It is just that Vim will interpret it to be having a width of 4.
set shiftwidth=4    " Indents will have a width of 4
set softtabstop=4   " Sets the number of columns for a TAB
set expandtab       " Expand TABs to spaces
set autoindent
set number          " Enable line numbers
set ignorecase
set smartcase


" ----- Plugin settings -----
let NERDTreeQuitOnOpen=1
let g:airline_powerline_fonts = 1
let g:airline_skip_empty_sections = 1

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" ----- Commands -----
:command Dtrails %s/\s\+$//e


" ----- Shortcuts -----
" fuzzy finder & plugins
" noremap     <C-f>       :GFiles<CR>
noremap     <C-g>       :Rg<CR>
noremap     <C-f>       :Files<CR>
" nnoremap    <C-b>       :Buffers<CR>
nnoremap    <leader>m   :Marks<CR>
nnoremap    <leader>gc  :GCheckout<CR>

" other
nnoremap    <silent>    <esc>   :noh<return><esc>

tnoremap <Esc> <C-\><C-n>
nmap        <silent>    <C-b>   :NERDTreeToggle<CR>

" red for trailing white spaces
highlight ExtraWhitespace ctermbg=red guibg=#ff5555
match ExtraWhitespace /\s\+$/

autocmd FileType tex set spell spelllang=en
autocmd FileType !tex set nospell
autocmd FileType qf setlocal nospell
set tw=78
set fo+=t " textwrapping an
set noswapfile

set scrolloff=5
set mouse=
" Das hier soll auch in Olpe sein!
" test test
"


